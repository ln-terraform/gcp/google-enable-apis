module "enable-apis" {
  source = "../.."

  project_id = "gcloud-309315"
  services = [
    "compute.googleapis.com",
    "container.googleapis.com",
  ]
}