variable "services" {
  type    = list(string)
  default = []
}

variable "project_id" {
  type    = string
  default = ""
}